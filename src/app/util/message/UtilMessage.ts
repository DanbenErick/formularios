import Swal from 'sweetalert2';

export function MensajeAlertError() {
  Swal.fire({
    type: 'error',
    title: 'Oops...',
    html: '<b>Algo salió mal!</b> <br> Si el error continua, comuniquese con el área de Sistemas.',
  });
}

export function MensajePhoenixInfo(mensaje: string) {
  Swal.fire({
    type: 'info',
    title: 'Phoenix Informativo',
    html: mensaje,
  });
}

export function MensajePhoenixErrorConLista(mensaje: any) {
  let msg: string = '';
  msg += '<ul>';
  for (let m of mensaje) {
    msg += `<li>${m.DE_ESTA}</li>`;
  }
  msg += '</ul>';

  Swal.fire({
    type: 'error',
    title: 'Phoenix Informativo',
    html: msg,
  });
}

export function MensajePhoenixError(mensaje: string) {
  Swal.fire({
    type: 'error',
    title: 'Phoenix Informativo',
    html: mensaje,
  });
}
