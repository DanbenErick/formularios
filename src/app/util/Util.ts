import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MensajePhoenixError } from './message/UtilMessage';

export async function setLoadingSpinner(
  spinner: any,
  observable: Observable<any>
) {
  spinner.show();
  observable
    .pipe(
      catchError((err) => {
        console.log(err);
        return throwError(err);
      })
    )
    .subscribe({
      next: (data) => {
        console.log(data);
      },
      error: (e) => {
        console.log('error:::', e);
        MensajePhoenixError(e + '');
        spinner.hide(); /*sólo si quiero que aparezca el error al usuario*/
      },
      complete: () => {
        console.log('complete');
        spinner.hide();
      },
    });
}
