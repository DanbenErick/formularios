export interface DocumentoCobrarResponse {
  compania: string;
  cliente: string;
  origen: string;
  tipo: string;
  n_documento: string;
  fecha_documento: string;
  fecha_contable: string;
  moneda: string;
  monto: string;
}
