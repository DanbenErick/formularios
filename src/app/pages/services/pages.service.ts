import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PagesService {
  private url ='http://localhost:3500/documento-cobrar/obtener-documento-cobrar';

  public constructor(private http: HttpClient) {}

  public obtenerDocumentosCobrar(): Observable<any> {
    return this.http.get<any>(this.url).pipe(
      delay(5000),
      map((response: any) => {
        return response;
      })
    );
  }
}
