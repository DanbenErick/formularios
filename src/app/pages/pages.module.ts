/*
  Modulos Angular
*/
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

/*
  PrimNG
*/
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { ButtonModule } from 'primeng/button';

/*
Componentes Propios
*/
import { DocumentoCobrarListadoComponent } from './documento-cobrar-listado/documento-cobrar-listado';


@NgModule({
  declarations: [
    DocumentoCobrarListadoComponent,
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    BrowserModule,

    PanelModule,
    TableModule,
    DropdownModule,
    CalendarModule,
    ButtonModule,
  ],
  exports: [DocumentoCobrarListadoComponent],
})
export class PagesModule {}
