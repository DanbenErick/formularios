/*
  Modulos Externos
*/
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

/*
  Modulos Propios
*/
import { PagesService } from '../services/pages.service';
import { setLoadingSpinner } from 'src/app/util/Util';

@Component({
  selector: 'app-documento-cobrar-listado',
  templateUrl: './documento-cobrar-listado.component.html',
  styleUrls: ['./documento-cobrar-listado.component.scss'],
})

export class DocumentoCobrarListadoComponent implements OnInit {
  public params: any = {};

  public formulario: any = {
    compania: [],
    cliente: [],
    origen: [],
    fecha_inicio: '',
    fecha_final: '',
    tipo: '',
  };
  public items: any[] = []

  public constructor(
    private pagesService: PagesService,
    private spinner: NgxSpinnerService
    ) {}

  ngOnInit() {
    this.spinner.show();
    this.obtenerListado(this.params);
  }

  private obtenerListado(params: any) {
    const lista = this.pagesService.obtenerDocumentosCobrar().pipe(
      map((response: any[]) => {
        console.log(response);
        this.items = response;
      })
    );
    setLoadingSpinner(this.spinner, lista);
  }
}
